# Default Team Name CS304 2016 Project

* Adam Woods            - 39241120
* Addison Bellamy-Gross - 18383133
* Steven Hall           - 12042131

### Setting up the db ###

# open mysql with our db name
sqlite3 app.sl3

# inside the prompt
mysql> .read database_start.sql

mysql> .exit

# gg

# Icons from Noun Project courtesy of
Piola
Denis Sazhin
Ismael Ruiz
