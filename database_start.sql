create table Tag  (
  tid integer,
  descriptor varchar(50),
  PRIMARY KEY (tid)
);

create table Describes  (
  tid integer,
  iid integer,
  PRIMARY KEY (tid, iid),
  FOREIGN KEY (tid) REFERENCES Tag,
  FOREIGN KEY (iid) REFERENCES Item
);

create table Item (
  iid integer,
  iname varchar(50),
  creation_date timestamp,
  price float,
  image_url varchar(300),
  PRIMARY KEY (iid)
);

create table Contains (
  iid integer,
  cid integer,
  FOREIGN KEY (cid) REFERENCES Has_Cart,
  FOREIGN KEY (iid) REFERENCES item
);

create table Has_Cart (
  cid integer,
  uid integer NOT NULL,
  PRIMARY KEY (cid),
  FOREIGN KEY (uid) REFERENCES Users
);

create table Users (
  uid integer,
  name varchar(50),
  login_name varchar(30),
  login_password varchar(255),
  PRIMARY KEY (uid)
);

create table Client (
  uid integer,
  card_type varchar(30),
  card_num varchar(50),
  PRIMARY KEY (uid),
  FOREIGN KEY (uid) REFERENCES Users
);

create table Admin (
  uid integer,
  PRIMARY KEY (uid),
  FOREIGN KEY (uid) REFERENCES Users
);

insert into Tag (tid, descriptor) values
	(1, "kitten"),
	(2, "old"),
	(3, "cute"),
	(4, "creepy"),
	(5, "funny"),
	(6, "calm"),
	(7, "combat"),
	(8, "friends"),
	(9, "costume"),
	(10, "sneaking"),
	(11, "hat"),
	(12, "food"),
	(13, "scared"),
	(14, "convicted felon");

insert into Describes (tid, iid) values
  (1, 1), (3, 1), (8, 1), (9, 1), (11, 1),
  (8, 2), (3, 2), (9, 2), (6, 2), (11, 2),
  (8, 3), (3, 3), (11, 3), (12, 3), (9, 3),
  (11, 4), (9, 4), (1, 4), (3, 4), (14, 4),
  (2, 5), (9, 5), (11, 5), (5, 5), (14, 5),
  (5, 6), (13, 6),
  (5, 7), (13, 7), (14, 7),
  (13, 8), (10, 8),
  (13, 9), (5, 9), (14, 9),
  (5, 10), (13, 10), (4, 10),
  (7, 11), (5, 11), (13, 11),
  (7, 12), (13, 12),
  (12, 13), (5, 13), (11, 13),
  (12, 14), (5, 14), (11, 14), (6, 14),
  (12, 15), (5, 15), (14, 15),
  (12, 16), (1, 16), (8, 16), (5, 16), (3, 16),
  (12, 17), (1, 17), (3, 17),
  (3, 18), (1, 18), (12, 18), (9, 18), (11, 18),
  (12, 19), (3, 19),
  (3, 20), (6, 20),
  (6, 21), (12, 21), (5, 21),
  (10, 22), (5, 22), (3, 22), (12, 22),
  (10, 23), (14, 23), (5, 23), (3, 23),
  (7, 24), (13, 24), (14, 24),
  (4 , 25),
  (3 , 26),
  (6 , 27),
  (3 , 28),
  (14 , 29),
  (6 , 30),
  (6 , 31),
  (10 , 32),
  (3 , 33),
  (5 , 34),
  (6 , 35),
  (13 , 36),
  (3 , 37),
  (14 , 38),
  (3 , 39),
  (4 , 40);

insert into Item (iid, iname, creation_date, price, image_url) values
  (1, "Cat frog hat", 1478919824, 6.99, "http://spotgator.com/wp-content/uploads/scrapecontent/cdn.lolwot.com_wp-content_uploads_2016_04_10-hilariously-adorable-photos-of-cats-with-hats-8.jpg"),
  (2, "bat cat", 1478914864, 6.99, "https://s-media-cache-ak0.pinimg.com/originals/aa/8f/12/aa8f1239a830bc88e6a691d62ab9effa.jpg"),
  (3, "watermoelon cat", 1478989864, 6.99, "https://media0.giphy.com/media/6J3j9EeO7oi7m/200_s.gif"),
  (4, "robin hood cat", 1478919822, 6.99, "http://i.dailymail.co.uk/i/pix/2015/11/05/21/2E0C3BDD00000578-0-image-a-50_1446760514552.jpg"),
  (5, "santa cat", 1478919833, 27.99, "http://slappedham.com/wp-content/uploads/2015/08/Cat-in-Santa-hat.jpg"),
  (6,  "big eyes", 1478913464, 23.99, "http://kittytonpost.com/wp-content/uploads/2013/10/Cat-Pictures-Funny.jpg"),
  (7, "terrified cat", 1478913864, 27.99, "http://vignette2.wikia.nocookie.net/creepypasta/images/0/0a/Scared_cat.jpg/revision/latest?cb=20141012235103"),
  (8, "scared cat", 1478911864, 63.50, "http://www.petcareintl.com/wp-content/uploads/2014/10/pets-owner.jpg"),
  (9, "surprised cat", 1478919164, 3.99, "https://i.ytimg.com/vi/d801FUQWHyg/hqdefault.jpg"),
  (10, "super soaker", 1478917364, 2.49, "https://tuxthedappergent.files.wordpress.com/2014/04/5545889af2b0f051f71a01bd4add4313.jpg?w=625&h=778"),
  (11, "hisser", 1478919862, 73.00, "http://www.clevercaption.com/uploadedImages/bud/_scared-cat.jpg"),
  (12, "protector", 1478919224, 8.49, "https://i.ytimg.com/vi/xYNafWePeHo/hqdefault.jpg"),
  (13, "bread head", 14789045864, 7.99, "https://i.ytimg.com/vi/g8du14oxTM8/hqdefault.jpg"),
  (14, "grumpy bread head", 1478913864, 6.99, "http://i.imgur.com/VlzlSow.jpg"),
  (15, "grape juice", 1478916864, 5.99, "https://img.buzzfeed.com/buzzfeed-static/static/enhanced/web05/2012/3/8/2/enhanced-buzz-821-1331192745-13.jpg"),
  (16, "friends forevs", 1478315861, 6.79, "http://endlessfuels.com/wp-content/uploads/2013/05/946835_483611171708865_1211186441_n.jpg"),
  (17, "trip to the market", 1478419862, 3.99, "http://petfoodia.com/wp-content/uploads/2011/10/Depositphotos_5307675_XS1-300x207.jpg"),
  (18, "bun head", 1478914860, 7.99, "https://s-media-cache-ak0.pinimg.com/564x/da/a9/dd/daa9dd93e27afabce7973a7c2a37c13d.jpg"),
  (19, "birthday treat", 1478619834, 5.99, "http://65.media.tumblr.com/tumblr_m7sjvlSEOh1qjc1a7o1_500.jpg"),
  (20, "chill bill", 1477919464, 5.99, "https://s-media-cache-ak0.pinimg.com/236x/5c/f9/d0/5cf9d0ff1052387ceb06524ef87bef3f.jpg"),
  (21, "cat napped", 1478919111, 4.79, "http://www.solofoods.com/sites/solofoods.com/files/cat-eating-cake-1.jpg"),
  (22, "sneak attack", 1478912224, 3.33, "https://dncache-mauganscorp.netdna-ssl.com/thumbseg/451/451973-bigthumbnail.jpg"),
  (23, "you can’t see me", 1478919333, 5.99, "https://s-media-cache-ak0.pinimg.com/236x/ee/66/83/ee668392a09ed8ba7f3f0607f4ca9ebe.jpg"),
  (24, "battle cat", 1478919444, 27.99, "http://24.media.tumblr.com/tumblr_lqgl5mUtVB1qaqjtoo1_500.jpg"),
	(25, "greyman", 1478919111, 19.99, "https://www.petdrugsonline.co.uk/images/page-headers/cats-master-header"),
	(26, "little buds", 1478919000, 29.99, "https://s-media-cache-ak0.pinimg.com/564x/29/9e/56/299e56ab07c75af6407289ecc4ab1dd6.jpg"),
	(27, "desert cat", 1478919123, 68.99, "https://upload.wikimedia.org/wikipedia/commons/9/9b/Gustav_chocolate.jpg"),
	(28, "blue eyes", 1478919234, 12.99, "https://s-media-cache-ak0.pinimg.com/736x/a7/72/89/a772896fd412b59aa1edb9acc604bb87.jpg"),
	(29, "power ears", 1478919345, 3.99, "http://static.wixstatic.com/media/7ee089_d393a068c8e84a19941ede40eabe2d10.jpg_srz_1405_1405_85_22_0.50_1.20_0.00_jpg_srz"),
	(30, "noble cat", 1478919432, 24.99, "http://valleyviewgrade5.weebly.com/uploads/3/8/4/0/38405589/4561286_orig.jpg"),
	(31, "shrubbery cat", 1478919112, 55.99, "http://static.boredpanda.com/blog/wp-content/uploads/2016/04/beautiful-fluffy-cat-british-longhair-22.jpg"),
	(32, "wisdom cat", 1478917654, 78.59, "http://cdn.paper4pc.com/images/cat-ears-whiskers-eyes-wallpaper-1.jpg"),
	(33, "british short hair tabber", 1478912543, 9.99, "http://static.boredpanda.com/blog/wp-content/uploads/2016/02/most-beautiful-eyes-cat-coby-british-shorthair-46.jpg"),
	(34, "living life", 1478911129, 19.99, "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSw2pcWKQuYdLP0WmiDenMqE347CX6gkz0xqFXZQizi7fMO4ajZ"),
	(35, "majestic", 1478913421, 32.99, "https://s-media-cache-ak0.pinimg.com/564x/16/de/fd/16defd6ea4a3a8d0e30801a446af0c69.jpg"),
	(36, "oh really", 1478912221, 22.99, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRJvndg_jSWr0x_Q8FznhaAemIVsFhT1qTD0Wnw8uWass3DekoReQ"),
	(37, "hello friend", 1478913241, 7.99, "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTUyT9PPyq0OMClyi1KKTH2MYcYyer41ZVOoDPcEC4CAoSZ9mywAQ"),
	(38, "literally a tiger", 1472912221, 14000.00, "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQ4oFz5_cJ6kv83hIp6wTOATVc9YiKDz9FZdXWZT-iWn_AGVnfn"),
	(39, "hey bud", 1478912239, 14.99, "http://www.rd.com/wp-content/uploads/sites/2/2016/02/06-train-cat-shake-hands.jpg"),
	(40, "body snatcher", 1478910001, 2.99, "http://www.rd.com/wp-content/uploads/sites/2/2016/04/08-cat-wants-to-tell-you-clothes.jpg");

insert into Has_Cart (cid, uid) values (1, 1), (2, 2), (3, 3), (4, 4);

insert into Users (uid, name, login_name, login_password) values
	(1, "Addison Bird-Gravel", "addisonbg", "$2y$10$afYQWK/aolSo.CgYWyCYIOct1cdlcUBl1Zt4RTJTDJnA4AieVEXna"),
	(2, "Adam Would", "adamw", "$2y$10$afYQWK/aolSo.CgYWyCYIOct1cdlcUBl1Zt4RTJTDJnA4AieVEXna"),
	(3, "Steven Hollandaise", "stevenh", "$2y$10$afYQWK/aolSo.CgYWyCYIOct1cdlcUBl1Zt4RTJTDJnA4AieVEXna"),
	(4, "Rando Calrissian", "randoc", "$2y$10$afYQWK/aolSo.CgYWyCYIOct1cdlcUBl1Zt4RTJTDJnA4AieVEXna");

insert into Client (uid, card_type, card_num) values (4, "Visa", "1408986700304534");

insert into Admin (uid) values (1), (2), (3);

-- create trigger free_shipping_trigger 
-- before insert on Contains
-- begin
  -- update Has_Cart
  -- set free_shipping = TRUE
  -- where cid = NEW.cid;
-- end;
