<!DOCTYPE html>
<html ng-app="app" ng-controller="MainController">
  <head>
    <title>cs304 Default Team Name</title>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="js/app.js"></script>
    <link rel="stylesheet" type="text/css" href="css/browse.css" />
    <link rel="stylesheet" type="text/css" href="css/navbar.css" />
    <link rel="stylesheet" type="text/css" href="css/cart.css" />
  </head>

  <body style="width: 100vw; height: 100vh; margin: 0px; overflow: hidden;">
    <?php include('views/navbar.php'); ?>
    <?php include('views/cart.php'); ?>
    <?php include('views/browse.php'); ?>
  </body>
</html>
