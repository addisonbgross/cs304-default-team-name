<?php
  $postdata = file_get_contents("php://input");
  $data = json_decode($postdata);

  if (!isset($data->action)) {
    echo json_encode("No action/function specified for item_query.php");
    exit();
  }

  // dispatch to proper function call
  switch($data->action) {
    case 'select_all_items':
      select_all_items();

    case 'select_items_by_tag':
      select_items_by_tag($data->arg);

    case 'select_items_by_name':
      select_items_by_name($data->arg);

    case 'select_items_by_tag_and_name':
      select_items_by_tag_and_name($data->arg, $data->arg2);

    case 'add_tag_to_item':
      add_tag_to_item($data->arg, $data->arg2);

    case 'try_login':
      try_login($data->arg, $data->arg2);

    case 'check_admin':
      check_admin($data->arg);

    case 'do_password_change':
      do_password_change($data->arg, $data->arg2, $data->arg3);

    case 'check_cid':
      check_cid($data->arg);

    case  'list_cart_items':
      list_cart_items($data->arg);

    case 'quantity_of_item':
      quantity_of_item($data->arg, $data->arg2);

    case 'add_item_to_cart':
      add_item_to_cart($data->arg, $data->arg2);

    case 'remove_item_from_cart':
      remove_item_from_cart($data->arg, $data->arg2);

    case 'checkout_cart':
      checkout_cart($data->arg);

    case 'item_info':
      item_info($data->arg);

    default:
      echo json_encode("Invalid function call for item_query.php: " +
                       $data->action);
      exit();
  }

  /*
   * SELECT * FROM Items
   */
  function select_all_items() {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query('SELECT * FROM Item');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Tag');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Describes');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
   * SELECT * FROM Items WHERE tag = Items' tag
   */
  function select_items_by_tag($tag) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT i.*
      FROM item i, (SELECT i.iid, t.tid
            FROM item i, describes d, tag t
            WHERE i.iid=d.iid AND d.tid=t.tid
           )
      WHERE tid IN (SELECT tid
                    FROM (SELECT tid
                          FROM tag
                          WHERE UPPER(descriptor) LIKE UPPER('%{$tag}')
                         )
                   )
      GROUP BY iid
      HAVING COUNT(*) = (SELECT COUNT (*)
                         FROM (SELECT tid
                               FROM tag
                               WHERE UPPER(descriptor) LIKE UPPER('%{$tag}')
                              )
                        );
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Tag');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Describes');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
   * SELECT * FROM Item WHERE name = Item name
   */
  function select_items_by_name($name) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT i.*
      FROM Item i
      WHERE UPPER(i.iname)
      LIKE UPPER('%{$name}%')
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Tag');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Describes');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
   * SELECT * FROM Item WHERE name = Items' name AND tag = Items' Tag
   */
  function select_items_by_tag_and_name($tag, $name) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT i.*
      FROM Item i, Tag t
      JOIN Describes d ON d.tid=t.tid AND i.iid=d.iid
      WHERE UPPER(i.iname)
      LIKE UPPER('%{$name}%')
      AND
      UPPER(t.descriptor)
      LIKE UPPER('%{$tag}%')
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Tag');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    $results = $db->query('SELECT * FROM Describes');
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
   * INSERT INTO Describes new Tag for Item
   */
  function add_tag_to_item($iid, $tid) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      INSERT INTO Describes (tid, iid)
      VALUES ({$tid}, {$iid})
    ");

    // if insert was a success
    if ($results) {
      $results = $db->query('SELECT * FROM Tag');
      while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
        $export[] = $row;
      }

      $results = $db->query('SELECT * FROM Describes');
      while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
        $export[] = $row;
      }
    }

    echo json_encode($export);
    exit();
  };

  function try_login($username, $password) {
    $db = new SQLite3('app.sl3');
    $export;

    // Get the password hash
    $results = $db->query("
      SELECT login_password
      FROM users
      WHERE login_name = '{$username}'
    ");
    if ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $hash = $row;
      $hash = $hash['login_password'];
    }

    // Check if the hash matches the password that was given
    if (password_verify($password, $hash)) {
      $results = $db->query("
        SELECT uid, name, login_name
        FROM users
        WHERE login_name = '{$username}'
        ");
      if ($row = $results->fetchArray(SQLITE3_ASSOC)) {
        $export = $row;
      }
    }

    echo json_encode($export);
    exit();
  };

  function check_admin($uid) {
    $db = new SQLite3('app.sl3');
    $export;

    $results = $db->query("
      SELECT *
      FROM admin
      WHERE uid = {$uid}
    ");
    if ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export = $row;
    }

    echo json_encode($export);
    exit();
  };

  function do_password_change($uid, $old, $new) {
    $db = new SQLite3('app.sl3');
    $export;

    // Get the password hash
    $results = $db->query("
      SELECT login_password
      FROM users
      WHERE uid = '{$uid}'
    ");
    if ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $hash = $row;
      $hashed_password = $hash['login_password'];
    }

    // Check if the hash matches the password that was given
    if (password_verify($old, $hashed_password)) {
      $new = password_hash($new, PASSWORD_DEFAULT);
      $results = $db->query("
        UPDATE users
        SET login_password='{$new}'
        WHERE uid={$uid} AND login_password='{$hashed_password}'
      ");
      // Do this if the query completed successfully... which it always should
      if ($results) {
        // Shows how many lines were updated. Should only ever be a 0 or 1
        echo $db->changes();
      }
      else {
        echo -1;
      }
    }

    exit();
  };

   /*
   * Finds the Cart ID associated with given user
   */
    function check_cid($uid) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT cid
      FROM Has_Cart
      WHERE uid = {$uid}
    ");
    if ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
  * Find all (distinct) items for a Cart
  */
  function list_cart_items($uid) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT distinct i.*
      FROM Item i, Contains c, Has_Cart hc
      WHERE c.cid = hc.cid
      AND i.iid = c.iid
      AND hc.uid = {$uid} 
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
  * Finds the quantity of a given item in the cart
  */
  function quantity_of_item($iid,$cid) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT count(iid)
      FROM Contains
      WHERE iid = {$iid} and cid = {$cid}
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }

    echo json_encode($export);
    exit();
  };

  /*
  * add new item to the cart (Contains)
  */
  function add_item_to_cart($iid, $uid) {
    $db = new SQLite3('app.sl3');
    $export;

    $results = $db->query("
      select hc.cid
      from Has_Cart hc
      where hc.cid = {$uid}
    ");

    if ($results) {
      $cid = $results->fetchArray(SQLITE3_ASSOC); 
      $cid = $cid['cid'];

      $results = $db->query("
        INSERT INTO Contains 
        VALUES ({$iid}, {$cid})
      ");
    }

    // if insert was a success
    if ($results) {
      $results = $db->query("SELECT * FROM Item WHERE iid = {$iid}");
      while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
        $export = $row;
      }
    }

    echo json_encode($export);
    exit();
  };

  /*
  * Delete single Item from the cart (Contains)
  */
  function remove_item_from_cart($iid, $uid) {
    $db = new SQLite3('app.sl3');

    $results = $db->query("
      select hc.cid
      from Has_Cart hc
      where hc.cid = {$uid}
    ");

    if ($results) {
      $cid = $results->fetchArray(SQLITE3_ASSOC); 
      $cid = $cid['cid'];

      $results = $db->query("
        delete from Contains
        where Contains.iid IN (
          select i.iid
          from Item i, Has_Cart hc
          where i.iid = {$iid}
          and hc.cid = {$cid}
        )
      ");
    }

    echo json_encode($results);
    exit();
  };

  function checkout_cart($uid) {
    $db = new SQLite3('app.sl3');

    $results = $db->query("
      select hc.cid
      from Has_Cart hc
      where hc.cid = {$uid}
    ");

    if ($results) {
      $cid = $results->fetchArray(SQLITE3_ASSOC); 
      $cid = $cid['cid'];

      $results = $db->query("
        delete from Contains
        where Contains.iid IN (
          select i.iid
          from Item i, Has_Cart hc
          where hc.cid = {$cid}
        )
      ");
    }

    echo json_encode($results);
    exit();
  };

  /*
  * Name, price and image_url of a given item
  */
  function item_info($iid) {
    $db = new SQLite3('app.sl3');
    $export = [];

    $results = $db->query("
      SELECT iname, price, image_url
      FROM Item
      WHERE iid ={$iid}
    ");
    while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
      $export[] = $row;
    }
    echo json_encode($export);
    exit();
  };

?>
