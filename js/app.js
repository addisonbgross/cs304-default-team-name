var app = angular.module('app', []);

app.directive('onError', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.onError) {
          attrs.$set('src', attrs.onError);
        }
      });
    }
  }
});

app.controller("MainController", ["$scope", "$http", function($scope, $http) {
  $scope.items                   = [];
  $scope.users                   = [];
  $scope.describes               = [];
  $scope.tags                    = [];
  $scope.current_tag             = "";
  $scope.username                = "";
  $scope.password                = "";
  $scope.isLoggedIn              = false;
  $scope.name                    = "";
  $scope.uid                     = "";
  $scope.incorrectLoginInfo      = false;
  $scope.amIAdmin                = false;
  $scope.changing_password       = false;
  $scope.password_change_success = false;
  $scope.password_change_fail    = false;


  $scope.processTags = function(response) {
    $scope.describes = response.data.filter(function(des) {
      if (des.tid && des.iid) {
        return true;
      } else {
        return false;
      }
    });

    $scope.tags = response.data.filter(function(tag) {
      if (tag.descriptor) {
        return true;
      } else {
        return false;
      }
    });

    // attach tags
    $scope.items.forEach(function(item) {
      item.tags = [];

      $scope.describes.forEach(function(des) {
        if (des.iid == item.iid) {
          $scope.tags.forEach(function(tag) {
            if (tag.tid == des.tid) {
              item.tags.push(tag.descriptor);
            }
          });
        }
      });
    });
  };

  $scope.processItems = function(response) {
    $scope.items = response.data.filter(function(item) {
      if (item.iname) {
        return true;
      } else {
        return false;
      }
    });

    $scope.users = response.data.filter(function(user) {
      if (user.uid) {
        return true;
      } else {
        return false;
      }
    });

    $scope.processTags(response);
  };

  $scope.hideCurrentTag = function() {
    $scope.current_tag = "";

    // get full item list again
    $http.post(
      'item_query.php',
      { action: 'select_all_items' }
    ).then($scope.processItems);

    // tell Nav to refilter by text if needed
    $scope.$broadcast('filterByText');
  };

  // get all items on load
  $http.post(
    'item_query.php',
    { action: 'select_all_items' }
  ).then($scope.processItems);
}]);

app.controller("NavController", ["$scope", "$rootScope", "$http", function($scope, $rootScope, $http) {
  $scope.main_title = "Ye Olde Cat Shoppe";

  $scope.$on('filterByText', function() {
    $scope.filterByText();
  });

  $scope.filterByText = function() {
    // filter by both tag and item name
    if ($scope.current_tag != "") {
      $http.post(
        'item_query.php',
        {
          action: 'select_items_by_tag_and_name',
          arg: $scope.$parent.current_tag,
          arg2: $scope.search_text
        }
      ).then($scope.$parent.processItems);

    // filter by item name
    } else {
      $http.post(
        'item_query.php',
        {
          action: 'select_items_by_name',
          arg: $scope.search_text
        }
      ).then($scope.$parent.processItems);
    }
  };

  $scope.isAdmin = function() {
    $http.post(
      'item_query.php',
      {
        action: 'check_admin',
        arg: $scope.$parent.uid
      }
      ).then(function(response) {
        if (response.data.uid != null) {
          $scope.$parent.amIAdmin = true;
        }
        else {
          $scope.$parent.amIAdmin = false;
        }
      });
  };

  $scope.login = function() {
    $http.post(
      'item_query.php',
      {
        action: 'try_login',
        arg: $scope.username,
        arg2: $scope.password
      }
      ).then(function(response) {
        if (response.data.uid != null) {
          $scope.$parent.isLoggedIn = true;
          $scope.$parent.name = response.data.name;
          $scope.$parent.uid = response.data.uid;
          $scope.isAdmin();
          $scope.incorrectLoginInfo = false;
          
          // fetch the logged in user's cart
          $rootScope.$broadcast('getUserCart');
        }
        else {
          $scope.incorrectLoginInfo = true;
        }
      });
  };

  $scope.logout = function() {
    $scope.$parent.amIAdmin           = false;
    $scope.incorrectLoginInfo = false;
    $scope.$parent.isLoggedIn         = false;
    $scope.$parent.name               = "";
    $scope.$parent.password           = "";
    $scope.$parent.uid                = "";
    $scope.$parent.username           = "";
  };
  $scope.show_change_password = function() {
    $scope.$parent.changing_password = true;
  };

  $scope.change_password = function() {
    $http.post(
      'item_query.php',
      {
        action: 'do_password_change',
        arg: $scope.$parent.uid,
        arg2: $scope.old_password,
        arg3: $scope.new_password
      }
      ).then(function(response) {
        // Password was changed properly
        if (response.data == 1) {
          $scope.$parent.password_change_success = true;
          $scope.$parent.password_change_fail    = false;
        }
        else {
          $scope.$parent.password_change_success = false;
          $scope.$parent.password_change_fail    = true;
        }
      });
      $scope.$parent.changing_password = false;
  };
}]);

app.controller("BrowseController", ["$scope", "$rootScope", "$http",
function($scope, $rootScope, $http) {
  $scope.add_tag_open = false;

  $scope.showTagList = function() {
    $scope.add_tag_open = true;
  };

  $scope.hideTagList = function() {
    $scope.add_tag_open = false;
  };

  $scope.addTag = function(iid, tid) {
    $scope.add_tag_open = true;

    $http.post(
      'item_query.php',
      {
        action: 'add_tag_to_item',
        arg: iid,
        arg2: tid
      }
    ).then(function(response) {
      if (response.data.length > 0) {
        $scope.$parent.processTags(response);
      } else {
        console.log("This Item already has tag: " + tid);
      }
    });
  };

  $scope.addToCart = function(iid) {
    $rootScope.$broadcast('addItemToCart', {data: iid});
  };

  $scope.filterByTags = function(tag) {
    $scope.$parent.current_tag = tag;
    $rootScope.$broadcast('filterByText');
  };
}]);

app.controller("CartController", ['$scope', '$http', function($scope, $http) {
  $scope.cart_contents = [];

  $scope.getUserCart = function() {
    if ($scope.$parent.uid) {
      $http.post(
        'item_query.php',
        {
          action: 'list_cart_items',
          arg: $scope.$parent.uid
        }
      ).then(function(response) {
        $scope.cart_contents = response.data;
      });
    } 
  };

  $scope.$on('getUserCart', function() {
    $scope.getUserCart();
  });

  $scope.addItemToCart = function(iid) {
    if ($scope.$parent.uid) {
      $http.post(
        'item_query.php',
        {
          action: 'add_item_to_cart',
          arg: iid,
          arg2: $scope.$parent.uid
        }
      ).then(function(response) {
        $scope.cart_contents.push(response.data);
      });
    } 
  };

  $scope.$on('addItemToCart', function(ev, args) {
    $scope.addItemToCart(args.data);
  });

  $scope.removeItemFromCart = function(item) {
    if ($scope.$parent.uid) {
      $http.post(
        'item_query.php',
        {
          action: 'remove_item_from_cart',
          arg: item.iid,
          arg2: $scope.$parent.uid
        }
      ).then(function(response) {
        if (response.data) {
          $scope.cart_contents = $scope.cart_contents.filter(function(i) {
            return i.iid != item.iid;
          });
        }
      });
    } 
  };

  $scope.checkoutCart = function() {
    if ($scope.$parent.uid) {
      $http.post(
        'item_query.php',
        {
          action: 'checkout_cart',
          arg: $scope.$parent.uid
        }
      ).then(function(response) {
        if (response.data) {
          $scope.cart_contents = [];
          alert("Thanks for purchasing our fine cat photographs!");
        }
      });
    } 
  };

  // money

  $scope.shipping_cost = 2.00;
  $scope.total_shipping = 0.00;
  $scope.cart_tally = 0.00;

  // calculate shipping per item
  $scope.calculateShipping = function() {
    $scope.total_shipping = $scope.cart_contents.reduce(function(total, item) {
      if (item.name == "Literally a tiger") {
        return total + ($scope.shipping_cost * 10.0);
      } else {
        return total + $scope.shipping_cost;
      }
    }, 0);

    return $scope.total_shipping;
  };

  // calculate total cart cost + shipping
  $scope.calculateCartTally = function() {
    $scope.cart_tally = $scope.cart_contents.reduce(function(total, item) {
      return total + item.price;
    }, 0);

    $scope.cart_tally += $scope.calculateShipping();
  };

  // initialize cart
  $scope.calculateCartTally();

  // update cart when contents change 
  $scope.$watch("cart_contents", function() {
    $scope.calculateCartTally();
  });
}]);

