#!/bin/bash

chmod 644 *.sl3
chmod 644 *.sql
chmod 755 *.php
chmod 644 *.md

# All Directories
chmod 755 */

cd css
chmod 644 *.css
cd ..

cd js
chmod 755 *.js
cd ..

cd views
chmod 755 *
cd ..

cd img
chmod 755 *
cd ..
