<!-- current items you're browsing -->
<div ng-controller="BrowseController" class="browse-item-list">
  <div ng-show="items.length == 0" class="browse-no-items">
    <h2>Sorry, There are no cats like that here!</h2>
  </div>

  <div ng-repeat="item in items track by $index" class="browse-item">
    <img class="browse-item-zoomer" src="{{ item['image_url'] }}" on-error="img/sad_cat.png" />

    <div ng-show="amIAdmin == true" ng-click="showTagList()" ng-mouseleave="hideTagList()" class="browse-add-tag-button" title="Add Tag">
      <div class="browse-tag-selector" ng-class="{ 'browse-tag-selector-visible': add_tag_open }">
        <div ng-repeat="tag in tags" ng-click="addTag(item.iid, tag.tid)" class="browse-selectable-tag">
          <p>{{ tag.descriptor }}</p>
        </div>
      </div>
    </div>

    <div ng-show="isLoggedIn == true" ng-click="addToCart(item.iid)" class="browse-purchase-button" title="Add To Cart"></div>

    <div ng-repeat="tag in item.tags" class="browse-tag">
      <p ng-click="filterByTags(tag)">{{ tag }}</p>
    </div>

    <div class="browse-item-data">
      <h3>{{ item['iname'] | lowercase }}</h3>
      <p>{{ item['creation_date'] | date:'medium' }}</p>
      <p><em>${{ item['price'] | number:2 }}</em></p>
    </div>
  </div>
</div>
