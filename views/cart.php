<!-- shopping cart status and contents -->
<div ng-controller="CartController" class="cart">
  <div class="cart-contents">
    <h4 ng-show="cart_contents.length == 0 && name != ''" style="text-align: center;">No items in cart yet!</h4>

    <div ng-repeat="item in cart_contents" style="margin: 4px 0;">

      <div class="cart-item">
        <img class="cart-item-image" src="{{ item['image_url'] }}" on-error="img/sad_cat.png" />

        <div>
          <p>{{ item.iname}}</p>
          <p>${{ item.price | number:2 }}</p>
        </div>

        <span ng-click="removeItemFromCart(item)" class="cart-item-remove">X</span>
      </div>
    </div>
  </div>

  <div class="cart-tally">
    <p>Shipping: ${{ total_shipping | number:2 }}</p>
    <h3>Total: ${{ cart_tally | number:2  }}</h3>
    <div ng-click="checkoutCart()" ng-show="isLoggedIn == true && cart_contents.length > 0" class="cart-checkout" alt="purchase!"></div>
  </div>
</div>

