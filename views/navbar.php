<!-- static navigation and info bar -->
<div ng-controller="NavController" class="navbar">
  <div class="navbar-heading-wrapper">
    <div class="navbar-icon">
      <h1>{{ main_title }}</h1>
    </div>
  </div>

  <div class="navbar-search-wrapper">
    <div class="navbar-current-tag" ng-click="$parent.hideCurrentTag();" ng-show="current_tag != ''">
      {{ current_tag }} <span class="navbar-current-tag-x">X</span>
    </div>

    <input ng-model="search_text" ng-change="filterByText()"
           class="navbar-search-input" placeholder="search by name" type="text"
    />
  </div>

  <div class="navbar-echo">
    <p ng-show="isLoggedIn == true">Welcome {{ name }}</p>
    <p ng-show="password_change_success == true">Password Successfully Changed</p>
    <p ng-show="password_change_fail == true">Failed To Change Password</p>
    <p ng-show="incorrectLoginInfo == true">Invalid Login Credentials</p>
  </div>

  <form id='login' ng-show="isLoggedIn == false" ng-submit="login()" method='post' accept-charset='UTF-8'>
    <input type='hidden' name='submitted' id='submitted' value='1'/>
    <input ng-model="username" placeholder="username" type="text" />
    <input ng-model="password" placeholder="password" type="password" />
    <input type='submit' name='Submit' value='Login' />
  </form>

  <form id='logout' ng-show="isLoggedIn == true" ng-submit="logout()" method='post' accept-charset='UTF-8'>
    <input type='hidden' name='submitted' id='submitted' value='1'/>
    <input type='submit' name='Submit' value='logout' />
  </form>
  <form id='show_change_password' ng-show="isLoggedIn == true && changing_password == false" ng-submit="show_change_password()" method='post' accept-charset='UTF-8'>
    <input type='hidden' name='submitted' id='submitted' value='1'/>
    <input type='submit' name='Submit' value='Change Password' />
  </form>
  <form id='change_password' ng-show="changing_password == true" ng-submit="change_password()" method='post' accept-charset='UTF-8'>
    <input type='hidden' name='submitted' id='submitted' value='1'/>
    <input ng-model="old_password" placeholder="Old Password" type="password" />
    <input ng-model="new_password" placeholder="New Password" type="password" />
    <input type='submit' name='Submit' value='Change Password' />
  </form>
</div>
